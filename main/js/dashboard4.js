/*
Template Name: Admin Press Admin
Author: Themedesigner
Email: niravjoshi87@gmail.com
File: js
*/
$(function () {
    "use strict";
    // ============================================================== 
    // Product chart
    // ============================================================== 
    Morris.Area({
        element: 'morris-area-chart2',
        data: [{
            period: '2010',
            Desktop: 0,
            Mobile: 0,
            
        }, {
            period: '2011',
            Desktop: 130,
            Mobile: 100,
            
        }, {
            period: '2012',
            Desktop: 30,
            Mobile: 60,
            
        }, {
            period: '2013',
            Desktop: 30,
            Mobile: 200,
            
        }, {
            period: '2014',
            Desktop: 200,
            Mobile: 150,
            
        }, {
            period: '2015',
            Desktop: 105,
            Mobile: 90,
            
        },
         {
            period: '2016',
            Desktop: 250,
            Mobile: 150,
           
        }],
        xkey: 'period',
        ykeys: ['Desktop', 'Mobile'],
        labels: ['Desktop', 'Mobile'],
        pointSize: 0,
        fillOpacity: 0.4,
        pointStrokeColors:['#b4becb', '#ffa261'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 0,
        smooth: true,
        hideHover: 'auto',
        lineColors: ['#b4becb', '#ffa261'],
        resize: true
        
    });
   // ============================================================== 
   // Morris donut chart
   // ==============================================================       
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Post 1",
            value: 8500,

        }, {
            label: "Post 2",
            value: 3630,
        }, {
            label: "Post 3",
            value: 4870
        }],
        resize: true,
        colors:['#DBB59A', '#B3AEB1', '#DCB1B6']
    });
    // ============================================================== 
    // sales difference
    // ==============================================================
    
    // ============================================================== 
    // sparkline chart
    // ==============================================================
    var sparklineLogin = function() { 
       $('#sparklinedash').sparkline([ 0, 5, 6, 10, 9, 12, 4, 9], {
            type: 'bar',
            height: '50',
            barWidth: '2',
            resize: true,
            barSpacing: '5',
            barColor: '#434F54'
        });
         $('#sparklinedash2').sparkline([ 0, 5, 6, 10, 9, 12, 4, 9], {
            type: 'bar',
            height: '50',
            barWidth: '2',
            resize: true,
            barSpacing: '5',
            barColor: '#A09C93'
        });
          $('#sparklinedash3').sparkline([ 0, 5, 6, 10, 9, 12, 4, 9], {
            type: 'bar',
            height: '50',
            barWidth: '2',
            resize: true,
            barSpacing: '5',
            barColor: '#DCB1B6'
        });
           $('#sparklinedash4').sparkline([ 0, 5, 6, 10, 9, 12, 4, 9], {
            type: 'bar',
            height: '50',
            barWidth: '2',
            resize: true,
            barSpacing: '5',
            barColor: '#635F56'
        });
       
   }
    var sparkResize;
 
        $(window).resize(function(e) {
            clearTimeout(sparkResize);
            sparkResize = setTimeout(sparklineLogin, 500);
        });
        sparklineLogin();
});

